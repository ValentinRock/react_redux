import React from 'react';

import Header from "./components/layout/Header/Header";

import "./App.scss";
import Container from "./components/layout/Container/Container";
import Categories from "./components/core/Categories/Categories";
import CardsGrid from "./components/core/CardsGrid/CardsGrid";

const App = () => {
    return (
        <>
         <Header/>
            <main>
                <Container>
                    <Categories/>
                </Container>

                <Container>
                    <CardsGrid/>
                </Container>
            </main>
        </>
    );
};

export default App;