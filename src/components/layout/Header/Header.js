import React from 'react';

import Container from "../Container/Container";

import "./Header.scss";

const Header = () => {
    return (
        <header className="header">
            <Container>
                <div className="header__logo">
                    <h2 className="heading">
                        Result App
                    </h2>
                </div>
            </Container>
        </header>
    );
};

export default Header;