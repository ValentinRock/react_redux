import React from 'react';

import "./Categories.scss";
import Category from "../../UI/Category/Category";

const Categories = () => {
    const CATEGORIES = [
        {
            id: 1,
            name: 'Reading',
            count: 3,
            color: "#08243f"
        },
        {
            id: 2,
            name: 'Work',
            count: 12,
            color: "#f28f3e"
        },
        {
            id: 3,
            name: 'Relax',
            count: 72,
            color: "#338af3"
        }
    ];

    return (
        <div className="categories">
            {CATEGORIES.map(category =>
                <Category
                    key={category.id}
                    name={category.name}
                    count={category.count}
                    color={category.color}
                />
            )}
        </div>
    );
};

export default Categories;