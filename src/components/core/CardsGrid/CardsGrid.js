import React from 'react';

import "./CardsGrid.scss";
import Card from "../../UI/Card/Card";

const CardsGrid = () => {
    const CARDS = [
        {
            id: 1,
            title: 'Super title for card !',
            category: 'Reading',
            description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium cum cupiditate dicta dolor dolores ',
            done: true,
            inProgress: false
        },
        {
            id: 2,
            title: 'Super title for card !',
            category: 'Reading',
            description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium cum cupiditate dicta dolor dolores ',
            done: false,
            inProgress: true
        },
        {
            id: 3,
            title: 'Super title for card !',
            category: 'Reading',
            description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium cum cupiditate dicta dolor dolores ',
            done: true,
            inProgress: true
        },
        {
            id: 4,
            title: 'Super title for card !',
            category: 'Reading',
            description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium cum cupiditate dicta dolor dolores ',
            done: false,
            inProgress: false
        },
        {
            id: 5,
            title: 'Super title for card !',
            category: 'Reading',
            description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium cum cupiditate dicta dolor dolores ',
            done: true,
            inProgress: false
        }
    ]


    return (
        <div className="cards-grid">
            {CARDS.map(card =>
                <Card
                    key={card.id}
                    title={card.title}
                    category={card.category}
                    description={card.description}
                    done={card.done}
                    inProgress={card.inProgress}
                />
            )}
        </div>
    );
};

export default CardsGrid;