import React from 'react';

import "./Category.scss";

const Category = ({icon, name, count, color}) => {
    return (
        <div className="category" style={{backgroundColor: color}}>
            <div className="category__icon">
                {/*<img src={icon} alt={`${name} icon`}/>*/}
            </div>

            <p className="big-text">
                {name}
            </p>

            <span className="category__count">
                {count} items
            </span>
        </div>
    );
};

export default Category;