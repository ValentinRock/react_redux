import React from 'react';

import "./Card.scss";

const Card = ({id, title, category, description, done, inProgress}) => {
    return (
        <div className={`card ${inProgress ? '' : 'card_disabled'}`}>
            <div className="card__header">
                <p className="big-text">
                    {title}
                </p>

                <input type="checkbox" checked={done} readOnly/>
            </div>

            <div className="card__inner">
                <p>
                    {description}
                </p>
            </div>


            <div className="card__bottom">
                <p className="middle-text">
                    {category}
                </p>
            </div>
        </div>
    );
};

export default Card;